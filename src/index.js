import React from 'react';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk';
import {Routing} from './routing'
import reducers from './reducers'
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension'
import { BrowserRouter as Router } from 'react-router-dom';

// import App from './App';
import Header from './components/header/Header'
import Grid from './components/helpers/Grid'

// import {storePlaylist} from './store';

// import * as serviceWorker from './serviceWorker';

import './assets/scss/index.scss'

// const store = createStore(reducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
const store = createStore(reducers,composeWithDevTools(applyMiddleware(thunk)));

// function playlists(state = [], action) {
//     if (action.type === 'ADD_TRACK') {
//         return [
//             ...state,
//             action.payload
//         ]
//     }
//
//     return state;
// }
//
// const store = createStore(playlists);

ReactDOM.render(
    <Provider store={store}>
        <Router>
            <Header/>
            <Routing/>
            <Grid/>
        </Router>
    </Provider>,
    document.getElementById('app')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
