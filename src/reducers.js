import { combineReducers } from 'redux';
import { routeReducer } from 'react-router-redux';

import tracks from './reducers/tracks';
import playlists from './reducers/playlists';
import social from './reducers/socials';

export default combineReducers({
    routing: routeReducer,
    tracks,
    playlists,
    social
});