// import React, {Component} from 'react';
// import { Provider } from 'react-redux';
import { createStore } from 'redux';

// class Test extends Component{
//     render() {
//
//     }
// }

const initialState = {
    tracks: [
        'Track 1',
        'Track 2'
    ],
    playlists: [
        'Playlist 1',
        'Playlist 2'
    ]
};

function playlists(state = initialState, action) {
    if (action.type === 'ADD_TRACK') {
        return {
            ...state,
            tracks: [...state.tracks, action.payload]
        }
    }

    return state;
}

const store = createStore(playlists, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

export default store;