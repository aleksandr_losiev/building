import React, {Component} from 'react';
import { Route } from "react-router-dom";
// import { syncHistoryWithStore } from 'react-router-redux';

import Home from './templates/Home';
import About from './templates/About';
import Projects from './templates/Projects';
import Blog from './templates/Blog';
import Contacts from './templates/Contacts';

const routes = [
    {
        path: '/',
        title: 'Home',
        component: Home,
        exact: true
    },
    {
        path: '/about',
        title: 'About',
        component: About,
        exact: false
    },
    {
        path: '/projects',
        title: 'Projects',
        component: Projects,
        exact: false
    },
    {
        path: '/blog',
        title: 'Blog',
        component: Blog,
        exact: false
    },
    {
        path: '/contact',
        title: 'Contacts',
        component: Contacts,
        exact: false
    },
];

class Routing extends Component{
    render() {
        return (
            <main>
                {routes.map((route, i) => (
                    <Route path={route.path} component={route.component} exact={route.exact} key={i}/>
                ))}
            </main>
        )
    }
}

export {Routing, routes};