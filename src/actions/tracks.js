let mockApiData = [
    {
        id: 1,
        title: 'Track 1'
    },
    {
        id: 2,
        title: 'Track 2'
    },
    {
        id: 3,
        title: 'Track 3'
    },
    {
        id: 4,
        title: 'Track 4'
    },
    {
        id: 5,
        title: 'Track 5'
    }
];

export const getTracks = () => dispatch => {
    setTimeout(() => {
        console.log('I got tracks');
        dispatch({type: 'FETCH_TRACKS_SUCCESS', payload: mockApiData})
    }, 2000)
};