const initialState = [
    'Playlist 1',
    'Playlist 2'
];

export default function playlists(state = initialState, action) {
    if (action.type === 'ADD_PLAYLIST') {
        return [
            ...state,
            action.payload
        ]
        // return {
        //     ...state,
        //     tracks: [...state.tracks, action.payload]
        // }
    }

    return state;
}