const socials = [
    {
        name: 'Facebook',
        faIconClass: 'fa fa-facebook',
        link: 'https://uk-ua.facebook.com/'
    },
    {
        name: 'Instagram',
        faIconClass: 'fa fa-instagram',
        link: 'https://www.instagram.com/'
    },
    {
        name: 'Twitter',
        faIconClass: 'fa fa-twitter',
        link: 'https://twitter.com/'
    },
    {
        name: 'Pinterest',
        faIconClass: 'fa fa-pinterest-p',
        link: 'https://www.pinterest.com/'
    },
    {
        name: 'YouTube',
        faIconClass: 'fa fa-youtube',
        link: 'https://www.youtube.com/'
    }
];

export default socials;