const initialState = [];

export default function playlists(state = initialState, action) {
    if (action.type === 'ADD_TRACK') {
        return [
            ...state,
            action.payload
        ];
        // return {
        //     ...state,
        //     tracks: [...state.tracks, action.payload]
        // }
    } else if (action.type === 'FETCH_TRACKS_SUCCESS') {
        return action.payload;
    }

    return state;
}