import React, { Component } from 'react';
import {Routing} from './routing';
import { connect } from 'react-redux'
import { getTracks} from './actions/tracks'

class App extends Component {
  addTrack() {
    // console.log('add track', this.trackInput.value);
    this.props.onAddTrack(this.trackInput.value);
    this.trackInput.value = '';
  }

  findTrack() {
      console.log('findTrack', this.searchInput.value);
      this.props.onFindTrack(this.searchInput.value);
  }
  render() {
    console.log(this.props.tracks);
      return (
          <>
              <h1>Main App</h1>
              <Routing/>
              <div>
                  <div>
                      <input type='text' ref={input => this.trackInput = input}/>
                      <button onClick={this.addTrack.bind(this)}>Add track</button>
                  </div>
                  <div>
                      <input type='text' ref={input => this.searchInput = input}/>
                      <button onClick={this.findTrack.bind(this)}>Find track</button>
                  </div>
                  <div>
                      <button onClick={this.props.onGetTracks}>Get tracks</button>
                  </div>
                <ul>
                    {this.props.tracks.map((track, i) => (
                        <li key={i}>{track.title}</li>
                    ))}
                </ul>
              </div>
          </>
      )
  }
}

export default connect(
    state => ({
        tracks: state.tracks
    }),
    dispatch => ({
        onAddTrack: (title) => {
            let payload = {
                id: Date.now().toString(),
                title
            };

            dispatch({
                type: 'ADD_TRACK',
                payload
            })
        },
        onFindTrack: (name) => {
            dispatch({
                type: 'FIND_TRACK',
                payload: name
            })
        },
        onGetTracks: () => {
            dispatch(getTracks())
        }
    })
)(App);
