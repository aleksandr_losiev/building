import React from 'react';
import HeaderLogo from './HeaderLogo';
import HeaderNavigation from './HeaderNavigation';
import HeaderSocial from './HeaderSocial';

function Header() {
    return (
        <header>
            <div>
                <HeaderLogo/>
            </div>
            <div className='d-flex'>
                <HeaderNavigation/>
                <HeaderSocial/>
            </div>
        </header>
    )
}

export default Header;