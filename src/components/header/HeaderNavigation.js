import React from 'react';
import { Link } from 'react-router-dom';
import { routes } from "../../routing";

function HeaderNavigation() {
    return (
        <ul className='header-navigation'>
            {routes.map((route, i) => (
                <li key={i}>
                    <Link to={route.path}>{route.title}</Link>
                </li>
            ))}
        </ul>
    )
}

export default HeaderNavigation

let appData = {};

appData.expenses = {
    a1: 'test',
    a2: 'test2'
};