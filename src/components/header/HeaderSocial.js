import React from 'react'
import socials from "../../reducers/socials";

function HeaderSocial() {
    return (
        <ul className='header-socials'>
            {socials.map((social, i) => (
                <li key={i}>
                    <a href={social.link}><i className={social.faIconClass} /></a>
                </li>
            ))}
        </ul>
    );
}

export default HeaderSocial