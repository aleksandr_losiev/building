import React from 'react';
import {Link} from 'react-router-dom'

function HeaderLogo() {
    return (
        <div className='logo'>
            <Link to='/'>Building</Link>
        </div>
    );
}

export default HeaderLogo;