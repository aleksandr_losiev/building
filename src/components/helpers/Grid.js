import React from 'react';

function Grid() {
    return (
        <section className='grid-lines'>
            <div className='container'>
                <div className='line'><span /></div>
                <div className='line'><span /></div>
                <div className='line'><span /></div>
                <div className='line'><span /></div>
            </div>
        </section>
    )
}

export default Grid;